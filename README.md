Vagrant managed GitLab on VirtualBox VM
=======================================

GitLab on CentOS 7

GitLab Username: root  
GitLab Password: 5iveL!fe

Setup
-----

Install VirtualBox

Install Vagrant

Install Ansible

Install Vagrant Landrush plugin

Clone vagrant-gitlab

    git clone https://gitlab.com/mheiges/vagrant-gitllab.git

Start

    cd vagrant-gitlab
    vagrant up

URL

    http://gitlab.apidb.org/